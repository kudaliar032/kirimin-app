export const cost = (data) => new Promise((resolve, reject) => {
  fetch('https://cors-anywhere.herokuapp.com/https://api.rajaongkir.com/starter/cost', {
    method: 'POST',
    headers: {
      key: process.env.RAJAONGKIR_KEY,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({...data, "courier": "jne"})
  })
    .then(res => {
      if (res.status !== 200) {
        reject(res);
      }

      res.json().then(data => {
        resolve(data);
      })
    }).catch(err => {
    reject(err);
  })
});