import $ from "jquery";
import {cost} from "./rest";

const fromCity = $('#from-city');
const toCity = $('#to-city');
const weightPacket = $('#weight');
const costResultElement = document.querySelector('cost-result');

$('#cost-form').on('submit', async e => {
  e.preventDefault();
  const origin = fromCity.val();
  const destination = toCity.val();
  const weight = weightPacket.val();
  checkCost('loading');

  try {
    const {rajaongkir} = await cost({origin, destination, weight});
    costResultElement.costs = rajaongkir;
    checkCost('stop');
  } catch (e) {
    checkCost('stop');
    alert('Terjadi kesalahan');
    console.log(e);
  }
});

const checkCost = (state) => {
  if (state === 'loading') {
    $('#cost-submit').html('<span class="spinner-border spinner-border-sm mx-2" role="status" aria-hidden="true"></span>Periksa Ongkir');
  } else {
    $('#cost-submit').html('Periksa Ongkir');
  }
}
