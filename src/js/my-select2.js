import $ from "jquery";
import city from "./city.json";

const data = $.map(city, obj => {
  obj.id = obj.id || obj.city_id;
  obj.text = obj.text || `${obj.type} ${obj.city_name}`;
  return obj;
});

$('#from-city').select2({
  placeholder: "Kota asal",
  allowClear: true,
  data: data,
  theme: 'bootstrap4'
})

$('#to-city').select2({
  placeholder: "Kota tujuan",
  allowClear: true,
  data: data,
  theme: 'bootstrap4'
})