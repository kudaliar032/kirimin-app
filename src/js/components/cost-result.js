import myNumeral from '../../js/my-numeral';
import $ from 'jquery';

class CostResult extends HTMLElement {
  set costs(rajaongkir) {
    this._rajaongkir = rajaongkir;
    this.render();
  }

  render() {
    const {origin_details: origin, destination_details: destination} = this._rajaongkir;
    const {weight} = this._rajaongkir.query;
    const {costs} = this._rajaongkir.results[0];

    this.innerHTML = `<h3>Tarif Kiriman</h3>
      <div class="row mt-4 bg-gray py-4 rounded-lg mb-4 align-items-center text-center m-0">
        <div class="mx-auto col-12 col-lg-4 my-2 my-lg-0"><h4 id="cost-origin" class="px-3">
          <i class="fa fa-map-marker-alt" style="color: #6B968B"></i> ${origin.type} ${origin.city_name}, ${origin.province}
        </h4></div>
        <div class="mx-auto col-12 col-lg-4 my-2 my-lg-0"><h4 id="cost-destination" class="px-3">
          <i class="fa fa-shipping-fast" style="color: #6B968B"></i> ${destination.type} ${destination.city_name}, ${destination.province}
        </h4></div>
        <div class="mx-auto col-12 col-lg-4 my-2 my-lg-0"><h4 id="cost-weight" class="px-3">
          <i class="fa fa-weight" style="color: #6B968B"></i> <span>${myNumeral(weight).format('0,0')}</span> gram
        </h4></div>
      </div>
      <table class="table text-center">
        <thead>
        <tr>
          <th>Jenis layanan</th>
          <th>Tarif</th>
          <th>Estimasi pengiriman</th>
        </tr>
        </thead>
        <tbody id="cost-results"></tbody>
      </table>`;

    const costResultsElement = $('#cost-results');
    costs.forEach(cost => {
      costResultsElement.append(`<tr>
        <td>${cost.service} (${cost.description})</td>
        <td>${myNumeral(cost.cost[0].value).format('$0,0')}</td>
        <td>${cost.cost[0].etd} hari</td>
      </tr>`);
    })
  }
}

customElements.define('cost-result', CostResult);