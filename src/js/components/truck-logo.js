import Truck from '../../img/truck.svg';

class TruckLogo extends HTMLElement {
  connectedCallback() {
    this.render();
  }

  render () {
    this.innerHTML = `<img src=${Truck} alt="logo"/>`;
  }
}

customElements.define('truck-logo', TruckLogo);