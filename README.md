# KIRIMIN APP

Application to check JNE postage between cities / districts in Indonesia with API from [Raja Ongkir](https://rajaongkir.com/)

## Development

- setup `.env`
- `npm install`
- `npm run dev`

## Production

- setup `.env`
- `npm install`
- `npm run build`
- on `dist` directory, serve using web service like nginx or apache